package testScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethods.APITrigger;
import commonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_test_script extends APITrigger {
	public static void execute() throws IOException {
		File logfolder = Utilities.create_folder("Post_API");
		Response res = Post_API_trigger(post_requestBody(), post_endpoint());
		Response resp = Post_API_trigger(post_requestBody_TC2(), post_endpoint());
		// Extract the status code
		int statuscode = res.statusCode();
		System.out.println(statuscode);
		for (int i = 0; i < 6; i++) {
			if (statuscode == 201) {
				// Fetch the response body parameters
				ResponseBody responseBody = res.getBody();
				System.out.println(responseBody.asString());
				Utilities.create_log_file("Post_API_TC1", logfolder, post_endpoint(), post_requestBody(),
						res.getHeaders().toString(), responseBody.asString());
				validate(responseBody);

				ResponseBody responseBody1 = resp.getBody();
				System.out.println(responseBody1.asString());
				Utilities.create_log_file("Post_API_TC2", logfolder, post_endpoint(), post_requestBody_TC2(),
						res.getHeaders().toString(), responseBody1.asString());
				validate1(responseBody1);
				break;
			} else {
				System.out.println("Status code found in iteration " + i + " is: " + statuscode
						+ ", which is not equal to the expected status code, hence, retrying");
			}
		}
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 6 times.");
	}

	public static void validate(ResponseBody responseBody) {
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);
		// Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(post_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		// Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		// Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
	}

	public static void validate1(ResponseBody responseBody) {
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		// Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(post_requestBody_TC2());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}
}
