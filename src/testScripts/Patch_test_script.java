package testScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethods.APITrigger;
import commonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_test_script extends APITrigger {
	public static void execute() throws IOException {
		File logfolder = Utilities.create_folder("Patch_API");
		Response res = Patch_API_trigger(patch_requestBody(), patch_endpoint());
		// Step 3: Extract the status code
		int statuscode = res.statusCode();
		System.out.println(statuscode);
		for (int i = 0; i < 6; i++) {
			if (statuscode == 200) {
				// Step 4: Fetch the response body parameters
				ResponseBody responseBody = res.getBody();
				System.out.println(responseBody.asString());
				Utilities.create_log_file("Patch_API_TC1", logfolder, patch_endpoint(), patch_requestBody(),
						res.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("The status code for iteration " + i + " is:" + statuscode
						+ ", which is not equal to the expected status code, hence, retrying.");
			}
		}
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 6 times.");
	}

	public static void validate(ResponseBody responseBody) {
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		// Step 5: Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(patch_requestBody());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// Step 6 : Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Step 7 : Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

}
