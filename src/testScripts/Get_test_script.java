package testScripts;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;

import commonMethods.APITrigger;
import commonMethods.Utilities;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Get_test_script extends APITrigger {
	public static void execute() throws IOException {

		File logfolder = Utilities.create_folder("Get_API");
		Response resp = Get_API_trigger(get_requestBody(), get_endpoint());
		
		int statuscode = resp.statusCode();
		System.out.println(statuscode);
		for (int i = 0; i < 6; i++) {
			if (statuscode == 200) {
				// Fetch the response body parameters
				ResponseBody responseBody = resp.getBody();
				System.out.println(responseBody.asString());
				Utilities.create_log_file("Get_API_TC1", logfolder, get_endpoint(), get_requestBody(),
						resp.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("The status code for iteration " + i + " is:" + statuscode
						+ ", which is not equal to the expected status code, hence, retrying.");
			}
		}
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 6 times.");
	}

	public static void validate(ResponseBody responseBody) {
		// Declare expected results of page parameters
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;
		// Declare the expected results
		int[] id = { 7, 8, 9, 10, 11, 12 };
		String[] email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };
		// Declare expected results of support json
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";
		// Fetch response body page parameters
		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");

		// Fetch Size of data array
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		// fetch response body support json
		String res_url = responseBody.jsonPath().getString("support.url");
		String res_text = responseBody.jsonPath().getString("support.text");

		// Validate per page parameters
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		// Validate data array
		for (int i = 0; i < sizeofarray; i++) {
			// declare expected results
			int exp_id = id[i];
			String exp_email = email[i];
			String exp_first_name = first_name[i];
			String exp_last_name = last_name[i];
			String exp_avatar = avatar[i];

			// fetch response body data array parameters
			int res_id = responseBody.jsonPath().getInt("data[" + i + "].id");
			String res_email = responseBody.jsonPath().getString("data[" + i + "].email");
			String res_first_name = responseBody.jsonPath().getString("data[" + i + "].first_name");
			String res_last_name = responseBody.jsonPath().getString("data[" + i + "].last_name");
			String res_avatar = responseBody.jsonPath().getString("data[" + i + "].avatar");

			// validating data array parameters
			Assert.assertEquals(res_id, exp_id, "Validation of id failed for json object at index : " + i);
			Assert.assertEquals(res_email, exp_email, "Validation of email failed for json object at index : " + i);
			Assert.assertEquals(res_first_name, exp_first_name,
					"Validation of first name failed for json object at index : " + i);
			Assert.assertEquals(res_last_name, exp_last_name,
					"Validation of last name failed for json object at index: " + i);
			Assert.assertEquals(res_avatar, exp_avatar, "Validation of avatar failed for json onject at index: " + i);

		}

		// Validate support json parameters
		Assert.assertEquals(res_url, exp_url, "Validation of support json url failed");
		Assert.assertEquals(res_text, exp_text, "Validation of support json text failed");
	}

}
