package testScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import commonMethods.APITrigger;
import commonMethods.Utilities;
import io.restassured.response.Response;

public class Delete_test_script extends APITrigger {
	public static void execute() throws IOException {
		File logfolder = Utilities.create_folder("Delete_API");

		Response resp = Delete_API_trigger(delete_requestBody(), delete_endpoint());
		int status_code = resp.statusCode();
		System.out.println(status_code);
		for (int i = 0; i < 6; i++) {
			
			if (status_code == 204) {
				Utilities.create_log_file("Delete_API_TC1", logfolder, delete_endpoint(), delete_requestBody(),
						resp.getHeaders().toString(), resp.getBody().asString());
				break;
			}
			else {
				System.out.println("Status code for iteraion " + i + " is:" + status_code
						+ " , which is not equal to the expected status code, hence, retrying the API");
			}
		}
		Assert.assertEquals(status_code, 204, "Correct status code not found even after retrying for 6 times");
	}

}
