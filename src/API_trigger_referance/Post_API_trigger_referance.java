package API_trigger_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Post_API_trigger_referance {

	public static void main(String[] args) {
		
		//Declare the needed variables
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		RestAssured.baseURI = hostname;
		
		String responseBody = given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
		System.out.println(responseBody);
		//Parse response body using JsonPath and extract the response body parameters
		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_id = jsp_res.getString("id");
		String res_createdAt = jsp_res.getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0,11);
		
		//Parse request body using JsonPath and extract the request body parameters
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		//generate expected date
		LocalDateTime currentdate = LocalDateTime.now();
		String expectedDate = currentdate.toString().substring(0,11);
		
		//Validating using TestNG assertions
		Assert.assertEquals(res_name, req_name, "Name in response body is not equal to name sent in request body.");
		Assert.assertEquals(res_job, req_job, "Job in response body is not equal to job sent in request body.");
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt, expectedDate, "Date in response body is not equal to the expected date");
		
		
		

	}

}
