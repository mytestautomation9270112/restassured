package API_trigger_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Patch_API_trigger_referance {

	public static void main(String[] args) {
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"john\",\r\n"
				+ "    \"job\": \"manager\"\r\n"
				+ "}";
		
RestAssured.baseURI = hostname ;
		
		String responseBody= given().header(headername, headervalue).body(requestBody).when().put(resource).then().extract().response().asString();
		System.out.println(responseBody);
		//parse the response body using JsonPath
			//Create Jsonpath object
		JsonPath jsp_res = new JsonPath(responseBody);
		
	    //Parse request body using JsonPath and extract the request body parameters
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_updatedAt = jsp_res.getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0,11);
		
		//Parse request body using JsonPath and extract the request body parameters
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		//generate expected date
		LocalDateTime currentDate = LocalDateTime.now();
		String expectedDate = currentDate.toString().substring(0,11);
		
		//validate using TestNG assertions
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expectedDate);

	}

}
