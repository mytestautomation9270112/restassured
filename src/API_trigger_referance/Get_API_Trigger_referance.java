package API_trigger_referance;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

public class Get_API_Trigger_referance {

	public static void main(String[] args) {

		// Declare expected results of page parameters
		int page = 2;
		int per_page = 6;
		int total = 12;
		int total_pages = 2;

		// Declare the expected results
		int[] id = { 7, 8, 9, 10, 11, 12 };
		String[] email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// Declare expected results of support

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		// Collect all needed information and save it into local variables
		String hostname = "https://reqres.in/";
		String resource = "/api/users?page=2";

		// Declare BaseURI
		RestAssured.baseURI = hostname;

		// Configure the API for execution and save the response in a String variable
		String responseBody = given().when().get(resource).then().extract().response().asString();
		System.out.println(responseBody);

		// parse the response body using JsonPath
		// Create the object of JsonPath
		JsonPath jsp_res = new JsonPath(responseBody);

		// Fetch the page parameters
		int res_page = jsp_res.getInt("page");
		int res_per_page = jsp_res.getInt("per_page");
		int res_total = jsp_res.getInt("total");
		int res_total_pages = jsp_res.getInt("total_pages");
		
		//validating page parameters
		Assert.assertEquals(res_page, page);
		Assert.assertEquals(res_per_page, per_page);
		Assert.assertEquals(res_total, total);
		Assert.assertEquals(res_total_pages, total_pages);
		
		//fetch support parameters
		String res_url = jsp_res.getString("support.url");
		String res_text = jsp_res.getString("support.text");

		// Fetch the response body data array length
		int size = jsp_res.getInt("data.size()");
		System.out.println("Length of array : " + size);

		for (int i = 0; i < size; i++) {
			// declare expected results
			String exp_id = Integer.toString(id[i]);
			String exp_email = email[i];
			String exp_firstname = first_name[i];
			String exp_lastname = last_name[i];
			String exp_avatar = avatar[i];

			// Fetch response body parameters
			// Parse individual params using jsp_res object
			String res_id = jsp_res.getString("data[" + i + "].id");
			System.out.println("Response body id: " + res_id);
			String res_email = jsp_res.getString("data[" + i + "].email");
			System.out.println("Response body email: " + exp_email);
			String res_firstname = jsp_res.getString("data[" + i + "].first_name");
			String res_lastname = jsp_res.getString("data[" + i + "].last_name");
			String res_avatar = jsp_res.getString("data[" + i + "].avatar");

			// Validating data array using TestNG assertions
			Assert.assertEquals(res_id, exp_id, "id assertion fail");
			Assert.assertEquals(res_email, exp_email, "email assertion fail");
			Assert.assertEquals(res_firstname, exp_firstname, "firstname assertion fail");
			Assert.assertEquals(res_lastname, exp_lastname, "lastname assertion fail");
			Assert.assertEquals(res_avatar, exp_avatar, "avatar assertion fail");

		}
		// validating support parameters
		Assert.assertEquals(res_url, exp_url);
		Assert.assertEquals(res_text, exp_text);

	}

}
