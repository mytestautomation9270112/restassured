package API_trigger_referance;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Delete_API_trigger_referance {

	public static void main(String[] args) {
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		
		RestAssured.baseURI = hostname;
		
		given().log().all().when().delete(resource).then().log().all().extract().response().asString();

	}

}
