package API_trigger_referance;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Delete_API_Reference_requestSpecification {

	public static void main(String[] args) {
		String hostname = "https://reqres.in/";
		String resource = "api/users/2";
		
		// Step 2: Trigger the API

		// Step 2.1 : Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();
		// Step 2.4 : Trigger the API
		Response resp = req_spec.delete(hostname + resource);
		System.out.println(resp);
		// Step 3: Extract the status code
		int statuscode = resp.statusCode();
		System.out.println(statuscode);

	}

}
