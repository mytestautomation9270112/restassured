package commonMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {

	public static File create_folder(String folder_name) {
		// fetch the current Java project
		String project_folder = System.getProperty("user.dir");
		// System.out.println(project_folder);

		// check if folder name coming in variable folder_name already exists in the
		// project_folder and create folder name accordingly
		File folder = new File(project_folder + "\\API_logs\\" + folder_name);
		if (folder.exists()) {
			System.out.println(folder + " already exists in Java Project: " + project_folder);
		} else {
			System.out.println(folder + " doesnt exist in Java Project: " + project_folder + " hence, creating it.");
			folder.mkdir();
			System.out.println(folder + " created in Java Project:" + project_folder);
		}
		return folder;
	}

	public static void create_log_file(String Filename, File File_location, String endpoint, String requestBody,
			String response_header, String responseBody) throws IOException {
		// create and open a text file
		File new_file = new File(File_location + "\\" + Filename + ".txt");
		// System.out.println("File created with name: " + new_file.getName());

		// write data into the created file
		FileWriter fw = new FileWriter(new_file);
		fw.write("Endpoint is : \n" + endpoint + "\n\n");
		fw.write("Request body is : \n" + requestBody + "\n\n");
		fw.write("Response header is : \n" + response_header + "\n\n");
		fw.write("Response body is : \n" + responseBody + "\n\n");

		// save and close the file
		fw.close();
	}

	public static ArrayList<String> ReadExcelData(String sheetname, String testcase) throws IOException {
		ArrayList<String> arrayData = new ArrayList<String>();
		String project_dir = System.getProperty("user.dir");
		FileInputStream fis = new FileInputStream(project_dir + "\\DataFiles\\InputData.xlsx");
		
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		int countofsheets = wb.getNumberOfSheets();
		// System.out.println(wb.getNumberOfSheets());
		for (int i = 0; i < countofsheets; i++) {
			if (wb.getSheetName(i).equals(sheetname)) {
				// System.out.println(wb.getSheetName(i));
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				while (rows.hasNext()) {
					Row datarows = rows.next();
					String testcaseName = datarows.getCell(0).getStringCellValue();
					// System.out.println(testcaseName);
					if (testcaseName.equals(testcase)) {
						Iterator<Cell> cellvalues = datarows.iterator();
						while (cellvalues.hasNext()) {
							String testdata = "";
							Cell cell = cellvalues.next();
							CellType datatype = cell.getCellType();
							if (datatype.toString().equals("STRING")) {
								testdata = cell.getStringCellValue();
							} else if (datatype.toString().equals("NUMERIC")) {
								Double num_testdata = cell.getNumericCellValue();
								testdata = String.valueOf(num_testdata);
							System.out.println(testdata);
							arrayData.add(testdata);
							}
						}
						break;
					}
				}
				break;
			} else {
				System.out.println("No sheet found of name : " + sheetname + " in current iteration : " + i);
			}
		}
		wb.close();
		return arrayData;
	}
}
