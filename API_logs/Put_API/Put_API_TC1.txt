Endpoint is : 
https://reqres.in/api/users/2

Request body is : 
{
    "name": "Sam",
    "job": "Developer"
}

Response header is : 
Date=Sat, 27 Apr 2024 00:25:28 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714177528&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=%2FUJfIbk99Hq1W2159nE5ELdIaXa0caToG6DL%2FpddMhE%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714177528&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=%2FUJfIbk99Hq1W2159nE5ELdIaXa0caToG6DL%2FpddMhE%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"47-3+9SD8qhAho4dqJzf7JbZxXdhAk"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Vary=Accept-Encoding
Server=cloudflare
CF-RAY=87aaacee2a989a8f-NAG
Content-Encoding=gzip

Response body is : 
{"name":"Sam","job":"Developer","updatedAt":"2024-04-27T00:25:28.305Z"}

