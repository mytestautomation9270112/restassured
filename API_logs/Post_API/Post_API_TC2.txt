Endpoint is : 
https://reqres.in/api/users

Request body is : 
{
    "name": "Raj@12",
    "job": "Manager"
}

Response header is : 
Date=Mon, 29 Apr 2024 15:26:18 GMT
Content-Type=application/json; charset=utf-8
Content-Length=82
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714404374&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=nUftEpRRL8lr8lck8CeHt5Ddm9cEfe%2FJWIsOfAIi6aA%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714404374&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=nUftEpRRL8lr8lck8CeHt5Ddm9cEfe%2FJWIsOfAIi6aA%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"52-/13YSHn+ZkynTeWQj2CrrQ9IRss"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87c04f24789d6bb7-SIN

Response body is : 
{"name":"Raj@12","job":"Manager","id":"659","createdAt":"2024-04-29T15:26:19.806Z"}

